library ieee;
use ieee.std_logic_1164.all;

entity ParallelPort is
	port(
		-- Avalon interfaces signals
		Clk : in std_logic;
		nReset : in std_logic;
		
		Address : in std_logic_vector(2 downto 0);
		ChipSelect : in std_logic;
	
		Read : in std_logic;
		Write : in std_logic;
	
		ReadData : out std_logic_vector(31 downto 0);
		WriteData : in std_logic_vector(31 downto 0);
	
		-- Parallel Port external interface
		ParPort : inout std_logic_vector(31 downto 0)
	);
end ParallelPort;

architecture comp of ParallelPort is

	signal iRegDir : std_logic_vector(31 downto 0);
	signal iRegPort : std_logic_vector(31 downto 0);
	signal iRegPin : std_logic_vector(31 downto 0);
begin
	pregWr: process(Clk, nReset)
	begin
		if nReset = '0' then
			iRegDir <= (others => '0');
		elsif rising_edge(Clk) then
			if ChipSelect = '1' and Write = '1' then
				case Address(2 downto 0) is
					when "000" => iRegDir <= WriteData;
					when "010" => iRegPort <= WriteData;
					when "011" => iRegPort <= iRegPort or WriteData;
					when "100" => iRegPort <= iRegPort and not WriteData;
					when others => null;
				end case;
			end if;
		end if;
	end process pRegWr;

	-- Read from registers with wait 0
--	ReadData <= iRegDir when Address = "000" else
--		    iRegPin when Address = "001" else
--		    iRegPort when Address = "010" else
--		    "00000000000000000000000000000000";

	-- Read Process from registers with wait 1
	pRegRd: process(Clk)
	begin
		if rising_edge(Clk) then
			ReadData <= (others => '0');
			if ChipSelect = '1' and Read = '1' then
				case Address(2 downto 0) is
					when "000" => ReadData <= iRegDir;
					when "001" => ReadData <= iRegPin;
					when "010" => ReadData <= iRegPort;
					when others => null;
				end case;
			end if;
		end if;
	end process pRegRd;
	
	-- Parallel Port output value
	pPort: process(iRegDir, iRegPort)
	begin
		for i in 0 to 31 loop
			if iRegDir(i) = '1' then
				ParPort(i) <= iRegPort(i);
			else
				ParPort(i) <= 'Z';
			end if;
		end loop;
	end process pPort;

	-- Parallel Port Input value
	iRegPin <= ParPort;
end comp;







