LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.all;

ENTITY LCD_control IS
	PORT( 
	-- Avalon interfaces signals
		Clk_i    	: IN std_logic;
		nReset_i 	: IN std_logic;
		
		Addres_i		: IN std_logic;
		ChipSelect_i: IN std_logic;
		Avalon_WR_i	: IN std_logic;
		Avalon_RD_i	: IN std_logic;
		Avalon_WR_data_i : IN std_logic_vector(31 DOWNTO 0);
		Avalon_RD_data_o : OUT std_logic_vector(31 DOWNTO 0);
		
		WaitReq_o	: OUT std_logic;
		
	-- Parallel Port external interface
		LCD_data_o 	: OUT std_logic_vector(15 DOWNTO 0);
		nLCD_RD_o 	: OUT std_logic;
		nLCD_WR_o	: OUT std_logic;
		--nLCD_ChipSelect_o: OUT std_logic;
		LCD_D_Cx_o	: OUT std_logic 	--when D_Cx = 0 => command is written to LCD
												--when D_Cx = 1 => data is written to LCD
		--nLCD_Reset_o: OUT std_logic;
		--LCD_IMO_o : OUT std_logic --can be done with GPIO;
	);
End LCD_control;

architecture comp of LCD_control is
		
		type state_t is (idle, WRdn, WRup);
		signal state : state_t;
		--signaux synchrones
		signal DATA2LCD_s : std_logic_vector(15 downto 0);
		signal DCx_s : std_logic;
		
begin
		--transfert des données au LCD
		LCD_data_o <= DATA2LCD_s;
		--transfert de l'adresse au LCD
		LCD_D_Cx_o <= DCx_s;


		processFSMLogic : process(Clk_i, nReset_i)
		begin
			if(nReset_i = '0') then
				
				state <= idle;
					
				WaitReq_o <= '0';
				nLCD_WR_o <= '1';
				DCx_s <= '0';
				DATA2LCD_s <= (others => '0');
				
				--unused signals		
				nLCD_RD_o <= '1';
				Avalon_RD_data_o <= (others => '0');
				
			elsif (rising_edge(Clk_i)) then
				
				Avalon_RD_data_o <= (others => '0');
				nLCD_RD_o <= '1';
				-- default values
				DCx_s <= DCx_s;
				DATA2LCD_s <= DATA2LCD_s;
								
				case state is
					when idle =>
					
						if(ChipSelect_i = '1' AND Avalon_WR_i = '1') then
							state <= WRdn;
							
							WaitReq_o <= '1';
							DCx_s <= Addres_i;
							nLCD_WR_o <= '0';
							DATA2LCD_s <= Avalon_WR_data_i(15 downto 0);
						else
							state <= state;
							
							WaitReq_o <= '0';
							nLCD_WR_o <= '1';
						end if;
						
					when WRdn =>
						state <= WRup;
						
						WaitReq_o <= '1';
						nLCD_WR_o <= '0';

					when WRup =>
						state <= idle;
						
						WaitReq_o <= '0';
						nLCD_WR_o <= '1';
				end case;				
			end if;		
		end process processFSMLogic;

end comp;