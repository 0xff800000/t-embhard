	SoPC u0 (
		.clk_clk                         (<connected-to-clk_clk>),                         //                  clk.clk
		.lcd_0_conduit_end_address       (<connected-to-lcd_0_conduit_end_address>),       //    lcd_0_conduit_end.address
		.lcd_0_conduit_end_writedata     (<connected-to-lcd_0_conduit_end_writedata>),     //                     .writedata
		.lcd_0_conduit_end_endofpacket_n (<connected-to-lcd_0_conduit_end_endofpacket_n>), //                     .endofpacket_n
		.lcd_0_conduit_end_write         (<connected-to-lcd_0_conduit_end_write>),         //                     .write
		.mygpio_0_conduit_end_export     (<connected-to-mygpio_0_conduit_end_export>),     // mygpio_0_conduit_end.export
		.reset_reset_n                   (<connected-to-reset_reset_n>),                   //                reset.reset_n
		.sdram_ctrl_wire_addr            (<connected-to-sdram_ctrl_wire_addr>),            //      sdram_ctrl_wire.addr
		.sdram_ctrl_wire_ba              (<connected-to-sdram_ctrl_wire_ba>),              //                     .ba
		.sdram_ctrl_wire_cas_n           (<connected-to-sdram_ctrl_wire_cas_n>),           //                     .cas_n
		.sdram_ctrl_wire_cke             (<connected-to-sdram_ctrl_wire_cke>),             //                     .cke
		.sdram_ctrl_wire_cs_n            (<connected-to-sdram_ctrl_wire_cs_n>),            //                     .cs_n
		.sdram_ctrl_wire_dq              (<connected-to-sdram_ctrl_wire_dq>),              //                     .dq
		.sdram_ctrl_wire_dqm             (<connected-to-sdram_ctrl_wire_dqm>),             //                     .dqm
		.sdram_ctrl_wire_ras_n           (<connected-to-sdram_ctrl_wire_ras_n>),           //                     .ras_n
		.sdram_ctrl_wire_we_n            (<connected-to-sdram_ctrl_wire_we_n>),            //                     .we_n
		.sram_clk_clk                    (<connected-to-sram_clk_clk>)                     //             sram_clk.clk
	);

